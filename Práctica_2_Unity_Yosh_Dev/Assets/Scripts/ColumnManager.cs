﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ColumnManager : MonoBehaviour
{
    public GameObject[] columnPrefabs;
    public GameObject currentColumn;

    private static ColumnManager columnManager_Instance;
    public static ColumnManager ColumnManager_Instance
    {        
        get
        {
            if (columnManager_Instance == null)
            {
                columnManager_Instance = GameObject.FindObjectOfType<ColumnManager>();
            }
            return columnManager_Instance;
        }
    }

    private Stack<GameObject> leftColumns = new Stack<GameObject>();
    public Stack<GameObject> LeftColumns
    {
        get { return leftColumns; }
        set { leftColumns = value; }
    }

    private Stack<GameObject> topColumns = new Stack<GameObject>();
    public Stack<GameObject> TopColumns
    {
        get { return topColumns; }
        set { topColumns = value; }
    }



    // Use this for initialization
    void Start()
    {
        CreateColumns(20);

        for (int i = 0; i < 15; i++)
        {
            SpawnColumn();
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void CreateColumns(int amount)
    {
        for (int i = 0; i < amount; i++)
        {
            leftColumns.Push(Instantiate(columnPrefabs[0]));
            TopColumns.Push(Instantiate(columnPrefabs[1]));

            leftColumns.Peek().name = "LeftColumn";
            leftColumns.Peek().SetActive(false);

            TopColumns.Peek().name = "TopColumn";                                 
            TopColumns.Peek().SetActive(false);
        }
    }

    public void SpawnColumn()
    {

        if (leftColumns.Count == 0 || TopColumns.Count == 0)
        {
            CreateColumns(10);
        }

        //Generando un numero aleatorio entre 0 y 1
        int randomIndex = Random.Range(0, 2);

        if (randomIndex == 0)
        {
            GameObject tmpColumn = leftColumns.Pop();
            tmpColumn.SetActive(true);
            tmpColumn.transform.position = currentColumn.transform.GetChild(0).transform.GetChild(randomIndex).position;
            currentColumn = tmpColumn;
        }
        else if (randomIndex == 1)
        {
            GameObject tmpColumn = TopColumns.Pop();
            tmpColumn.SetActive(true);
            tmpColumn.transform.position = currentColumn.transform.GetChild(0).transform.GetChild(randomIndex).position;
            currentColumn = tmpColumn;
        }

        int spawnPickup = Random.Range(0, 10);

        if (spawnPickup == 0)
        {
            currentColumn.transform.GetChild(1).gameObject.SetActive(true);
        }
        //LeftColumn or TopColumn
        //currentColumn = Instantiate(columnPrefabs[randomIndex], currentColumn.transform.GetChild(0).transform.GetChild(randomIndex).position, Quaternion.identity);            
    }

    public void ResetGame()
    {
        SceneManager.LoadScene("firstScene");                
    }

    public void ExitGame()
    {
        Application.Quit();
    }
}
