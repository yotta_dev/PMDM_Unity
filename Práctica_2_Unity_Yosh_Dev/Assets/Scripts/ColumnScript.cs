﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColumnScript : MonoBehaviour {

    public float fallDelay = 0.1f;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            ColumnManager.ColumnManager_Instance.SpawnColumn();
            StartCoroutine(FallDown());
            //Debug.Log("Spawn Column");
        }
    }

    IEnumerator FallDown() {
        yield return new WaitForSeconds(fallDelay);
        GetComponent<Rigidbody>().isKinematic = false;
        yield return new WaitForSeconds(2);

        switch (gameObject.name)
        {
            case "LeftColumn":
                ColumnManager.ColumnManager_Instance.LeftColumns.Push(gameObject);
                gameObject.GetComponent<Rigidbody>().isKinematic = true;
                gameObject.SetActive(false);
                break;

            case "TopColumn":
                ColumnManager.ColumnManager_Instance.TopColumns.Push(gameObject);
                gameObject.GetComponent<Rigidbody>().isKinematic = true;
                gameObject.SetActive(false);
                break;
        }
    }

}
