﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class BallScript : MonoBehaviour {
    public float speed;

    public Rigidbody rigidbody;
    public GameObject particleSystem;
    public GameObject resetButton;
    
    public Animator gameOverAnimator;
    private int score = 0;

    public LayerMask isGround;    
    public Transform contactPoint;
 
    public Text actualScoreText;
    public Text lastScoreText;
    public Text bestScoreText;
    public Text newHighScoreText;
    public Text initialText;

    private Vector3 dir;
    private Vector3 initialPosition;
    private bool isDead = false;
    private bool isPlaying = false;

    private int numberMatches = 0;

    // Use this for initialization
    void Start ()
    {
        initialPosition = transform.position;
        dir = Vector3.zero;

        numberMatches = PlayerPrefs.GetInt("Partidas");

        if (numberMatches > 0)
        {            
            initialText.gameObject.SetActive(false);
            rigidbody.useGravity = true;
            transform.Translate((speed * Time.deltaTime) * Vector3.forward);
        }
        
    }
	
	// Update is called once per frame
	void Update ()
    {
        //Si no esta tocando el suelo esta muerto
        if (!IsInGround() && isPlaying) {
            //Kill Player
            isDead = true;

            GameOver();

            numberMatches++;
            PlayerPrefs.SetInt("Partidas", 0);

            resetButton.SetActive(true);
            rigidbody.useGravity = false;
        }
        

        if (Input.GetMouseButtonDown(0) && !isDead)
        {
            initialText.gameObject.SetActive(false);

            rigidbody.useGravity = true;

            if (dir == Vector3.forward)
            {
                dir = Vector3.left;
            }
            else {
                dir = Vector3.forward;
            }
        }
        //Calculo y ejecucion Direcccion
        transform.Translate((speed * Time.deltaTime) * dir);
    }


    void OnTriggerEnter(Collider other)
    {
       if(other.tag == "Pickup")
        {
            other.gameObject.SetActive(false);
            Instantiate(particleSystem, transform.position, Quaternion.identity);
            //Puntuacion Extra
            score += 5;
            actualScoreText.text = score.ToString();
        }
    }

    //Para saber si al salir de un Collider el Siguiente Collider es una columna o no
    //En el caso de que no sea una Columna el jugador ha perdido.
    void OnTriggerExit(Collider other)
    {
        if (other.tag == "Column")
        {
            RaycastHit hit;
            Ray downRay = new Ray(transform.position, -Vector3.up);

            if (Physics.Raycast(downRay, out hit))
            {
                score++;                
                actualScoreText.text = score.ToString();
            }            
        }  
    }

    private void GameOver()
    {
        gameOverAnimator.SetTrigger("GameOver");

        lastScoreText.text = actualScoreText.text;

        int bestScore = PlayerPrefs.GetInt("BestScore", 0);

        if (score > bestScore)
        {
            PlayerPrefs.SetInt("BestScore", score);
            newHighScoreText.gameObject.SetActive(true);
        }

        bestScoreText.text = PlayerPrefs.GetInt("BestScore",0).ToString();

    }

    private bool IsInGround()
    {
        //Creamos un pequeño collider alimentado por el punto de contacto
        //pasado por Unity como un GameObject vacio posicionado por debajo de 
        //la Esfera para detectar si la esfera ha tocado el suelo o no
        Collider[] colliders = Physics.OverlapSphere(contactPoint.position, .5f, isGround);

        for (int i = 0; i < colliders.Length; i++)
        {
            //Es aqui donde comprobamos si la Esfera sigue tocando el Suelo o no
            if(colliders[i].gameObject != gameObject)
            {
                isPlaying = true;
                return true;
            }
        }

        return false;
    }
}
