﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/* This script is responsible for detecting if the Esc key has been 
 * pressed so that based on the specific Scene that is active in, an action its taken
 * or the other in this case the action of returning from the Game to the Cover and of the Cover
 * to close the Game is realized.
 */
public class ExitButton : MonoBehaviour {
    //I declare an Object Scene thanks to using the UnityEngine.SceneManagement to be able to
    //get the information we need to evaluate the Condition.
    private Scene scene;

    // Use this for initialization
    // At the beginning of the script we store the execution of the function .GetActiveScene() 
    // in the Object Scene to know what Scene is active.
    void Start () {
		scene = SceneManager.GetActiveScene();
        //Debug.Log("Active Scene is '" + scene.name + "'.");
    }

    // Update is called once per frame
    /* When calling this method, we will detect if the Escape key has been pressed
     * and based on that we will have two Dynamics that are executed.
     * If the Scene is the cover, when you give the Intro it will close the Application
     * If the Scene is the game, it will return to the home screen.
     * 
     */
    void Update () {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            switch (scene.name)
            {
                case "pong_yosh_dev_v.0":
                    SceneManager.LoadScene("pong_yosh_dev_cover");
                    break;
                case "pong_yosh_dev_cover":                
                    Application.Quit();
                    //Debug.Log("Se ha terminado");
                    break;
            }
            

        }
    }
}
