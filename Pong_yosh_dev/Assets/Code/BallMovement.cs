﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* This Script stores information about the behavior of the ball.
 * We store the variables of speed, an Object RigidBody on which
 * we load from Unity the RigidBody of the ball, another Transform Object 
 * on which we load from Unity the Transform of the shovel, a boolean that 
 * tells us if it is playing or not, a Vector3 in which we store through 
 * the Transform of the ball that we can access through this script since 
 * we have assigned from Unity the Script to the 3D Ball Object and another 
 * Vector3 in which we store the information that will give the initial impulse 
 * to the ball .
 */
public class BallMovement : MonoBehaviour {
    public float initialSpeed=400f;
    public Rigidbody rig;
    public Transform shovel;
    public bool inGame=false;
    public Vector3 initialPosition;
    Vector3 initialImpulse;

    // Use this for initialization
    /* We store in the Vector Initial position the coordinates in the X, Y, Z 
     * axis the position of the Transform that we obtain when associating this 
     * Script with Unity to the GameObject Ball
     */
    void Start () {
        initialPosition = transform.position;
        initialImpulse = initialImpulse = new Vector3(-initialSpeed, initialSpeed, 0);
    }

    // Reset is called when the script is attached and not in playmode.
    /* When this method is called it will change the variable boolean inGame 
     * to False and to the RigidBody of the ball passed as parameter by assignment 
     * in Unity we launch the function isKinematic and we set it to true and 
     * we also use the addforce function passing as a Vector3.zero parameter. 
     * To stop the initial impulse that keeps the ball moving.
     * And to the Transform of the ball we assign the value of the Vector 
     * Initial Position to place the Ball back in its initialPosition. 
     */
    public void Reset()
    {
        inGame = false;
        rig.isKinematic = true;
        rig.AddForce(Vector3.zero);
        transform.position = initialPosition;           
    }

    // Update is called once per frame
    /* We control using Input.GetKeyDown if the LeftCtrl key has been pressed 
     * and if the Boolean variable inGame is false if this is True we will 
     * change the inGame variable to true, and the Transform of the ball using
     * the setParent function will set it to null so that it stops be the ball
     * daughter of the shovel, later in execution time when executing the 
     * SubstractLife method of the LivesManagement script after launching 
     * the Reset methods of the shovel and the ball will return to be daughter
     * of the shovel.
     */
    void Update () {
        if (Input.GetKeyDown(KeyCode.DownArrow) && inGame == false)
        {
            inGame = true;
            transform.SetParent(null);
            rig.isKinematic = false;
            rig.AddForce(initialImpulse,ForceMode.Force);
        }
    }
}
