﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShovelMovement : MonoBehaviour
{
    public float speed = 400f;
    public Vector3 initialPosition;

    // Use this for initialization
    void Start()
    {
        initialPosition = transform.position;

    }

    public void Reset()
    {
        transform.position = initialPosition;
    }


    // Update is called once per frame
    void Update()
    {
        //Almacenar Posicion Actual de la Pala
        Vector3 currentPosition = transform.position;

        //Almacenar la Direccion Introducida por Teclado
        float movementDirection = Input.GetAxisRaw("Vertical");

        //Calcular Distancia de movimiento
        float nextPosition = movementDirection * speed * Time.deltaTime;

        //Limiter of Range Shovel Movement
        if (currentPosition.y >= 1.25)
            currentPosition.y = 1.25f;
        else if (currentPosition.y <= -1.25)
            currentPosition.y = -1.25f;

        //Realizar Movimiento
        switch ((int)movementDirection)
        {
            case 1:
                transform.position = new Vector3(currentPosition.x, currentPosition.y + nextPosition, 0);
                break;
            case -1:
                transform.position = new Vector3(currentPosition.x, currentPosition.y + nextPosition, 0);
                break;
        }

    }
}
